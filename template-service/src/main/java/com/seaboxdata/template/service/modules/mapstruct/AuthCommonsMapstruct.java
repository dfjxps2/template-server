package com.seaboxdata.template.service.modules.mapstruct;

import com.seaboxdata.authuc.api.vo.local.OauthRoleCommonVO;
import com.seaboxdata.template.api.vo.RoleVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AuthCommonsMapstruct {

    AuthCommonsMapstruct INSTANCE = Mappers.getMapper(AuthCommonsMapstruct.class);

    RoleVO toRoleVO(OauthRoleCommonVO commonVO);

}
