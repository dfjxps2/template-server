package com.seaboxdata.template.service.modules.service;

import com.seaboxdata.template.api.vo.RoleVO;

import java.util.List;

public interface TemplateRoleService {

    List<RoleVO> querRoleVOs();

}
